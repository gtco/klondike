local anchor = base:extend()

function anchor:new(label, x, y)
    self.label = label
    self.x = x
    self.y = y
    local img = "res/card_outline.png"
    self.sprite = love.graphics.newImage(img)
    self.spritewidth = self.sprite:getWidth()
    self.spriteheight = self.sprite:getHeight()
    self.cards = {}
end

function anchor:draw()
    love.graphics.draw(self.sprite, self.x, self.y)
    love.graphics.rectangle("line", self.x, self.y, self.spritewidth, self.spriteheight)
    if self.cards ~= nil then
        for _, c in ipairs(self.cards) do
            c:draw()
        end
    end
end

function anchor:getPosition()
    return self.x, self.y
end

function anchor:addCard(card)
    -- offset bounds each time a card is added
    local card_count = self:getCardCount()
    local last_card = self.cards[card_count]
    if last_card ~= nil then
        local offset = card:getLowerBoundY() - last_card:getLowerBoundY()
        if offset ~= 0 then
            self.spriteheight = self.spriteheight + offset
        end
    end

    table.insert(self.cards, card)
end

function anchor:removeTopCard()
    local card_count = self:getCardCount()
    if card_count > 0 then
        local card = table.remove(self.cards, card_count)
        print("removeTopCard " .. self.label .. " " .. card.suit .. "" .. card.rank)
        return card
    end
end

function anchor:selectTopCard()
    local card_count = self:getCardCount()
    if card_count > 0 then
        local card = self.cards[card_count]
        card:set_selected(true)
        print("selectTopCard " .. self.label .. " " .. card.suit .. "" .. card.rank)
    end
end

function anchor:peekTopCard()
    local card_count = self:getCardCount()
    if card_count > 0 then
        return self.cards[card_count]
    else
        return nil
    end
end

function anchor:flipTopCard()
    local card_count = self:getCardCount()
    if card_count > 0 then
        local card = self.cards[card_count]
        if card ~= nil then
            card:flip()
        end
    end
end

function anchor:isSelected(mouseX, mouseY)
    --local x = lume.count(self.cards)
    local bx = self.x + self.spritewidth
    local by = self.y + self.spriteheight
    return mouseX > self.x and mouseX < bx and
        mouseY > self.y and mouseY < by
end

function anchor:getCardCount()
    local i = 0
    for _ in ipairs(self.cards) do i = i + 1 end
    return i
end


return anchor

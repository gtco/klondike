---@diagnostic disable: lowercase-global
lume = require "lib/lume"
base = require "lib/classic"
card = require "card"
anchor = require "anchor"
lovebird = require "lib/lovebird"
move = require "move"

local debug_draw_cards = true
local debug_random_seed = false
local seed = 999
local origin = nil

if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
    require("lldebugger").start()
end

function love.load()
    Suits = { "clubs", "diamonds", "hearts", "spades" }
    Ranks = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" }
    Deck = {}
    Anchors = {}
    createCardDeck()
    createAnchors()
    Moves = {}
    if debug_random_seed then
        love.math.setRandomSeed(seed)
    end
    shuffleDeck()
    dealCards()
end

function love.update()
    lovebird.update()
    for _, c in ipairs(Deck) do
        c:update()
    end
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end

function love.draw()
    love.graphics.setBackgroundColor(lume.color("rgb(58,148,110)"))
    for _, v in ipairs(Anchors) do
        v:draw()
    end
end

function love.mousepressed(x, y, button, istouch, presses)
    if button == 1 then -- Versions prior to 0.10.0 use the MouseConstant 'l'
        local stock = getAnchor("stock")
        if stock:isSelected(x,y) and stock:getCardCount() > 0 then
            local card = stock:removeTopCard()
            stock:flipTopCard()
            addCardToAnchor("waste", card)
        else
            origin = getSelectedAnchor(x,y)
            for _, c in ipairs(Deck) do
                c:checkSelected(x,y)
            end
        end
    end
 end

function love.mousereleased( x, y, button, istouch, presses )
    if button == 1 then
        local destination = getSelectedAnchor(x,y)
        if destination ~= nil and origin ~= nil then
            if origin.label == "waste" then
                moveFromWaste(origin, destination)
            elseif startsWith(origin.label, "f") then
                moveFromFoundation(origin, destination)
            elseif startsWith(origin.label, "c") then
                moveFromColumn(origin, destination)
            else
                print("unknown move")
            end
        end
        for _, c in ipairs(Deck) do
            handleCardMove(c)
        end
    end
end

function moveFromWaste(o, d)
    local a = o:peekTopCard()
    local b = d:peekTopCard()

    if startsWith(d.label, "f") then
        print("move from waste to foundation")
    elseif startsWith(d.label, "c") then
        print("move from waste to column")
    end
end

function moveFromFoundation(o, d)
    local a = o:peekTopCard()
    local b = d:peekTopCard()

    if startsWith(d.label, "f") then
        print("move from foundation to foundation")
    elseif startsWith(d.label, "c") then
        print("move from foundation to column")
    end
end

function moveFromColumn(o, d)
    local a = o:peekTopCard()
    local b = d:peekTopCard()

    if startsWith(d.label, "f") then
        print("move from column to foundation")
    elseif startsWith(d.label, "c") then
        print("move from column to column")
    end
end


function startsWith(label, prefix)
	return string.sub(label, 1, 1) == prefix
end

function getSelectedAnchor(x, y)
    for _, a in ipairs(Anchors) do
        if a:isSelected(x,y) then
            return a
        end
    end
    return nil
end


function handleCardMove(card)
    if card:getSelected() == true then
        --print("changing selection for " .. card.suit .. "" .. card.rank)
        card:setSelected(false)
        card:resetPosition()
    end
end

function dealCards()
    local cols = {"c1", "c2", "c3", "c4", "c5", "c6", "c7"}
    local count = 0
    local offset = 0
    -- deal to columns
    for i = 1,8 do
        offset = 20 * (i-1)
        for j, v in ipairs(cols) do
            count = count + 1
            local card = Deck[count]
            local anchor = getAnchor(v)
            local ax, ay = anchor:getPosition()
            card:setPosition(ax, ay + offset)
            anchor:addCard(card)
            print(count .. " " .. i .. " " .. j .. " " .. v .. " " .. card.suit .. "" .. card.rank)
        end
        table.remove(cols,1)
    end
    -- deal to stock
    count = count + 1
    local stock = getAnchor("stock")
    local sx, sy = stock:getPosition()
    local card = Deck[count]
    while (card ~= nil)
    do
        card:setPosition(sx, sy)
        stock:addCard(card)
        print(count .. " " .. card.suit .. "" .. card.rank)
        count = count + 1
        card = Deck[count]
    end
    -- deal to waste
    card = stock:removeTopCard()
    addCardToAnchor("waste", card)
    -- flip last card in each column
    for _, a in ipairs(Anchors) do
        a:flipTopCard()
    end
end

 function addCardToAnchor(label, card)
    local anchor = getAnchor(label)
    local ax, ay = anchor:getPosition()
    card:setPosition(ax, ay)
    anchor:addCard(card)
 end

function getAnchor(label)
    for _, a in ipairs(Anchors) do
        if a.label == label then
            return a
        end
    end
end

function shuffleDeck()
    local i = 0
    for _ in ipairs(Deck) do i = i + 1 end
    while (i > 0)
    do
        local j = love.math.random(1, i)
        local t = Deck[i]
        Deck[i] = Deck[j]
        Deck[j] = t
        i = i - 1
    end
end

function createAnchors()
    table.insert(Anchors, anchor("stock", 32, 32))
    table.insert(Anchors, anchor("waste", 132, 32))
    for i = 1, 4 do
        local f = anchor("f" .. i, 232 + (i*100) , 32)
        table.insert(Anchors, f)
    end
    for i = 1,7 do
        local c = anchor("c" .. i, 32 + ((i-1)*100), 160)
        table.insert(Anchors, c)
    end
end

function createCardDeck()
    for i = 1, #Suits do
        for j = 1, #Ranks do
            table.insert(Deck, card(Suits[i], Ranks[j], 10 + 80 * (j - 1), 90 * (i - 1)))
        end
    end
end
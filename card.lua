local card = base:extend()

function card:new(suit, rank, x, y)
    self.suit = suit
    self.rank = rank
    self.x = x
    self.y = y
    local i = "res/" .. self.rank .. "_" .. self.suit .. "_white.png"
    local b = "res/back_blue_basic_white.png"
    self.sprite = love.graphics.newImage(i)
    self.sprite_back = love.graphics.newImage(b)
    self.spritewidth = self.sprite:getWidth()
    self.spriteheight = self.sprite:getHeight()
    self.spritehalfwidth = self.spritewidth / 2
    self.spritehalfheight = self.spriteheight / 2
    self.is_selected = false
    self.is_facing_up = false
    self.last_x = x
    self.last_y = y
end

function card:draw()
    if self.is_facing_up then
        love.graphics.draw(self.sprite, self.x, self.y)
    else
        love.graphics.draw(self.sprite_back, self.x, self.y)
    end
end

function card:checkSelected(mouseX, mouseY)
    if (self.is_facing_up) then
        local bx = self.x + self.spritewidth
        local by = self.y + self.spriteheight
        self.is_selected = mouseX > self.x and mouseX < bx and
            mouseY > self.y and mouseY < by
        self.last_x = self.x
        self.last_y = self.y
    end
end

function card:setSelected(selected)
    self.is_selected = selected
end

function card:getSelected()
    return self.is_selected
end

function card:update()
    if self.is_selected then
        local dx, dy =  love.mouse.getPosition()
        self.x = dx - self.spritehalfwidth
        self.y = dy - self.spritehalfheight
    end
end

function card:resetPosition()
    self.x = self.last_x
    self.y = self.last_y
end

function card:getPositionY()
    return self.y
end

function card:getLowerBoundY()
    return self.y + self.spriteheight
end

function card:setPosition(x,y)
    self.x = x
    self.y = y
end

function card:flip()
    local f = not self.is_facing_up
    self.is_facing_up = f
end

function card:getSpriteHeight()
    return self.spriteheight
end

function card:isBlack()
    return self.suit == "clubs" or self.suit == "spades"
end

function card:isRed()
    return self.suit == "diamonds" or self.suit == "hearts"
end

function card:isAlternateColor(first, second)
    return first.isBlack() ~= second.isBlack()
end

function card:inSequence(lower, higher)
    --TODO
end

function card:placedAtBottom(parent, child)
    return self.isAlternateColor(parent, child) and self.inSequence(child, parent)
end

function card:sameSuit(first, second)
    return first.suit == second.suit
end

function card:placedOnFoundation(parent, child)
    return self.sameSuit(parent,child) and self.inSequence(parent, child)
end



return card

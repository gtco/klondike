local move = base:extend()

function move:new(x,y,mouse_presses)
    self.startx = x
    self.starty = y
    self.mouse_presses = mouse_presses
    self.cards = {}
end

function move:addCard(card)
    table.insert(self.cards, card)
end

function move:printDebug()
    print(self.startx, self.starty, self.mouse_presses)
end

return move
